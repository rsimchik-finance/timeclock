import calendar
import csv
from datetime import date, datetime, timedelta
from decimal import Decimal
from math import ceil, floor
from multiprocessing.sharedctypes import Value
import os
from re import template
import sys
from time import mktime, strftime, strptime
from xml.dom.domreg import well_known_implementations

time_format = '%I:%M:%S %p'
date_format = '%m/%d'
date_full_format = '%m/%d/%Y'
hourly_dollars = 60.0

def get_weeks_in_month(year, month):
	num_weekdays = 7
	first_day_weekday = date(year, month, 1).weekday()
	days_in_month = calendar.monthrange(year, month)[1]
	return ceil((days_in_month + first_day_weekday) / num_weekdays)

def get_week_index(year, month, day):
	num_weekdays = 7
	first_day_weekday = date(year, month, 1).weekday()
	return floor((day + first_day_weekday) / num_weekdays)

def get_timecard_weekday_html(timecard, day_index):
	html = '<th scope="row">{}</th>'.format(calendar.day_name[day_index])
	for week in timecard['Daily Hours']:
		num_hours = week[day_index]
		if num_hours == None:
			html += '<td class="inactive"></td>'
		else:
			html += '<td>{}</td>'.format(num_hours)

	return html

def get_timecard_body(timecard):
	body_html = "<tbody>"
	for day in range(7):
		body_html += "<tr>{}</tr>".format(get_timecard_weekday_html(timecard, day))
	body_html += "</tbody>"

	return body_html

def get_timecard_headers(timecard):
	html = '<thead><tr>'
	html += '<th scope="row">{}</th>'.format(calendar.month_name[timecard['Month']])
	for i in range(get_weeks_in_month(timecard['Year'], timecard['Month'])):
		html += '<th scope="col">Week {}</th>'.format(i + 1)
	html += '</tr></thead>'
	return html

def get_timecard_footer(timecard):
	html = '<tfoot><tr>'
	html += '<th scope="row">Total weekly hours</th>'
	for week_sum in timecard['Weekly Hours']:
		html += '<td>{}</td>'.format(week_sum)
	html += '</tr><tr>'
	html += '<th class="foot" scope="row">{} total hours</th>'.format(calendar.month_name[timecard['Month']])
	html += '<td>{}</td>'.format(timecard['Monthly Hours'])
	html += '</tr></tfoot>'
	return html

def get_timecard_html(timecard):
	timecard_html = "<main><table>"
	timecard_html += get_timecard_headers(timecard)
	timecard_html += get_timecard_body(timecard)
	timecard_html += get_timecard_footer(timecard)
	timecard_html += "</table></main>"

	return timecard_html

def populate_timecard_model(timecard):
	timeclock_path = get_full_path('Timeclock', 'csv', 'Month', date(timecard['Year'], timecard['Month'], 1))
	with open(timeclock_path, 'r') as timeclock_file:
		reader = csv.DictReader(timeclock_file)
		for row in reader:
			clock_date = strptime(row['Date'], date_format)
			day_index = clock_date.tm_mday - 1
			week_index = get_week_index(timecard['Year'], timecard['Month'], day_index)
			weekday_index = (day_index + timecard['First Day Offset']) % 7
			hours = Decimal(row['Hours Elapsed'])
			dollars = hours * Decimal(hourly_dollars)
			timecard['Daily Hours'][week_index][weekday_index] += hours
			timecard['Weekly Hours'][week_index] += hours
			timecard['Weekly Dollars'][week_index] += dollars
			timecard['Monthly Hours'] += hours
			timecard['Monthly Dollars'] += dollars

def generate_timecard_model(year, month):
	num_weekdays = 7
	first_day_weekday = date(year, month, 1).weekday()
	days_in_month = calendar.monthrange(year, month)[1]
	weeks_in_month = ceil((days_in_month + first_day_weekday) / 7.0)
	timecard = {
		'Year': year,
		'Month': month,
		'First Day Offset': first_day_weekday,
		'Daily Hours': [[0 for i in range(num_weekdays)] for j in range(weeks_in_month)],
		'Daily Dollars': [[0 for i in range(num_weekdays)] for j in range(weeks_in_month)],
		'Weekly Hours': [0 for i in range(weeks_in_month)],
		'Weekly Dollars': [0 for i in range(weeks_in_month)],
		'Monthly Hours': 0,
		'Monthly Dollars': 0
	}

	# trim days not in month
	for i in range(first_day_weekday):
		timecard['Daily Hours'][0][i] = None
		timecard['Daily Dollars'][0][i] = None
	
	last_day_weekday = date(year, month, days_in_month).weekday()
	for i in range(last_day_weekday + 1, num_weekdays):
		timecard['Daily Hours'][weeks_in_month - 1][i] = None
		timecard['Daily Dollars'][weeks_in_month - 1][i] = None

	timeclock_path = get_full_path('Timeclock', 'csv', 'month', date(year, month, 1))
	if os.path.isfile(timeclock_path):
		populate_timecard_model(timecard)

	return timecard

def render_timecard(year, month):
	timecard = generate_timecard_model(year, month)
	template_path = './template.html'
	timecard_path = get_full_path('Timecard', 'html', 'month', date(year, month, 1))
	with open(template_path) as template_file:
		with open(timecard_path, 'w') as timecard_file:
			for line in template_file.readlines():
				timecard_file.write(line.replace('$TIMECARD_CONTENT', get_timecard_html(timecard)))
			print('Saved at: file:///{}'.format(timecard_path));

def log(message):
	log_text = '[{}] {}'.format(datetime.now().strftime('%I:%M %p'), message)
	with open(get_full_path('Log', 'txt'), 'a') as log_file:
		log_file.write(log_text + '\n')
	print(log_text)

def clock(in_out):
	now = datetime.now()

	if not (in_out == 'in' or in_out == 'out'):
		raise ValueError('Unrecognized direction "{}". Clock "in" OR "out"?'.format(in_out))

	create_dir_if_none(get_file_path('Timeclock'))
	path = get_full_path('Timeclock', 'csv', 'Month')
	header = ['Date', 'Time In', 'Time Out', 'Hours Elapsed']
	rows = []

	if os.path.isfile(path):
		with open(path, 'r', newline='', encoding='utf-8') as clock_file:
			reader = csv.DictReader(clock_file)
			header = reader.fieldnames
			rows = [row for row in reader]

	log('clocking ' + in_out)
	if in_out == 'in':
		clock_in(header, rows, now, path)
	elif in_out == 'out' and len(rows) > 0:
		clock_out(header, rows, now, path)
	
def clock_in(csv_header, csv_rows, now, clock_file_path):
	row = { k: '' for k in csv_header }
	row['Date'] = now.strftime(date_format)
	row['Time In'] = now.strftime(time_format)
	csv_rows.append(row)
	write_timeclock_rows(clock_file_path, csv_header, csv_rows)

def clock_out(csv_header, csv_rows, now, clock_file_path):
	row = csv_rows[-1]

	# handle over-midnight work
	date_in = row['Date'] + '/' + now.year.__str__()	# as string
	date_in = strptime(date_in, date_full_format) # as time struct
	date_in = datetime.fromtimestamp(mktime(date_in)) # as datetime
	if now.date() > date_in.date():
		today_start = datetime.combine(now, datetime.min.time())
		yesterday_end = today_start - timedelta(seconds=1)
		clock_out(csv_header, csv_rows, yesterday_end, clock_file_path)
		clock_in(csv_header, csv_rows, today_start, clock_file_path)
		row = csv_rows[-1]

	row['Time Out'] = now.strftime(time_format)
	date_in = datetime.strptime(row['Date'], date_format)
	time_in = datetime.strptime(row['Time In'], time_format)
	datetime_in = datetime(now.year, date_in.month, date_in.day, 
						time_in.hour, time_in.minute, time_in.second)
	hours_elapsed = (now - datetime_in).total_seconds() / (60.0 ** 2)
	row['Hours Elapsed'] = str(round(hours_elapsed, 2))
	csv_rows[-1] = row
	write_timeclock_rows(clock_file_path, csv_header, csv_rows)

def write_timeclock_rows(file_path, csv_header, csv_rows):
	with open(file_path, 'w', newline='', encoding='utf-8') as clock_file:
		writer = csv.DictWriter(clock_file, csv_header)
		writer.writeheader()
		for row in csv_rows:
			writer.writerow(row)

def create_dir_if_none(path):
	if not os.path.isdir(path):
		os.mkdir(path, mode=0o750)

def get_file_path(file_type):
	return '{}/{}s'.format(base_path, file_type.title())

def get_file_name(file_type, file_extension, granularity='day', date=datetime.now()):
	file_type = file_type.title()
	file_extension = file_extension.strip('.')
	file_prefix = os.path.basename(base_path)

	granularity = granularity.lower()
	if granularity == 'day':
		date_format = '%Y-%m-%d'
	elif granularity == 'month':
		date_format = '%Y-%m'
	else:
		date_format = ''
	file_date = date.strftime(date_format)

	return '{}-{}-{}.{}'.format(file_prefix, file_type, file_date, file_extension)

def get_full_path(file_type, file_extension, granularity='day', date=datetime.now()):
	return '{}/{}'.format(get_file_path(file_type), get_file_name(file_type, file_extension, granularity, date))



if not len(sys.argv) == 2:
	print('usage: {} "path/to/work/folder"'.format(sys.argv[0]))
	exit(1)

base_path = sys.argv[1].rstrip('/')
if not os.path.isdir(base_path):
	print('The path you entered does not exist.')
	if input('Create? (y or n) ') == 'y':
		os.mkdir(base_path, mode=0o750)
	else:
		exit(1)

is_running = True
while is_running:
	user_input = input('> ')
	if user_input == 'exit':
		is_running = False
		continue

	command_index = user_input.find(' ')
	if command_index > 0:
		command = user_input[:command_index].lower()
		rest_of_command = user_input[command_index + 1:]
	else:
		command = user_input
		rest_of_command = ''

	if command == 'log':
		log(rest_of_command)
	elif command == 'clock':
		try:
			clock(rest_of_command)
		except ValueError as err:
			print(err)
	elif command == 'render':
		year_month = rest_of_command.split(' ')
		if len(year_month) < 2:
			print('Usage: render YYYY MM')
		else:
			render_timecard(int(year_month[0]), int(year_month[1]))
	else:
		print('Unrecognized command "{}"'.format(command))